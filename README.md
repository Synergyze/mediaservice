# README #

This is a demonstration repository with a basic program that reads information from two media services and parses them
into a queryable single media service.

### What is this repository for? ###
* This repo serves as sample code set for basic problem solving and programming skills.

### How do I get set up? ###

* This tool was built with the community edition of IntelliJ IDEA.
* The code can be compiled using the latest version of Maven, it will import the required dependencies.


### Dependencies ###
* These are: org.springframework.boot and com.google.code.gson
* SpringBoot has been used since it enables very easy RESTful service creation and configuration.
* GSON is used because it is a framework that allows for easy parsing of JSON strings.


### Configuration ###
* Included is a Configuration.xml file in which the used services are configured.
* The tool is hardcoded to send out GET calls to the configured external services.
* In the URL the "{0}" charactersequence is used to replace the query terms in the url.

### Usage ###

Try the healthcheck by calling the url without any additional methods.
Example healthcheck:
Call:
* http://localhost:8080/

returned JSON:
[{
    "serviceType" : "AlbumService",
    "healthStatus" : "Healthy",
    "message" : "",
    "responseTime" : 92
},{
    "serviceType" : "BookService",
    "healthStatus" : "Healthy",
    "message" : "",
    "responseTime" : 456
}]

Try the search method by calling "search" on the service with a query parameter.
The query value should be a url encoded value. E.g. spaces are encoded to a plus (+).

example search:
Call:
* http://localhost:8080/search?query=_searchterms_
* http://localhost:8080/search?query=five+finger

returned JSON:
[
{"title":"American Capitalist","type":"Album","author":"Five Finger Death Punch"},
{"title":"Five Finger Boogie","author":"David Carr Glover","type":"Book"},
{"title":"Five Finger Prayer Book","author":"","type":"Book"},
{"title":"Five finger exercise","author":"Peter Shaffer","type":"Book"},
{"title":"Five-Finger Discount","author":"Helene Stapinski","type":"Book"},
{"title":"Five-Finger Fun","author":"Lynn Freeman Olson","type":"Book"},
{"title":"Got Your Six (Deluxe)","type":"Album","author":"Five Finger Death Punch"},
{"title":"The Way of the Fist","type":"Album","author":"Five Finger Death Punch"},
{"title":"The Wrong Side of Heaven and the Righteous Side of Hell, Vol. 1","type":"Album","author":"Five Finger Death Punch"},
{"title":"The Wrong Side of Heaven and the Righteous Side of Hell, Vol. 2","type":"Album","author":"Five Finger Death Punch"}
]