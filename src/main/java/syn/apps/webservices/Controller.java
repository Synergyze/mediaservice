package syn.apps.webservices;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import syn.apps.configuration.Configuration;
import syn.apps.daos.Media;
import syn.apps.daos.HealthCheck;
import syn.apps.services.MediaSearchService;
import syn.apps.services.MediaService;
import syn.apps.services.implementation.AlbumService;
import syn.apps.services.implementation.BookService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Patrick on 04-Jul-17.
 */
@org.springframework.stereotype.Controller
@EnableAutoConfiguration
public class Controller {

    private MediaSearchService searchService;

    public Controller() {

        List<MediaService> mediaServices = new ArrayList<MediaService>();

        Configuration configuration = Configuration.getInstance();

        mediaServices.add(new AlbumService(configuration.albumServiceURL));
        mediaServices.add(new BookService(configuration.bookServiceURL));

        searchService = new MediaSearchService(mediaServices);
    }


    @RequestMapping(value = "/", produces = "application/json")
    @ResponseBody
    public List<HealthCheck> healthCheck(){

        return searchService.HealthCheck();
    }


    @RequestMapping(value="search", produces="application/json")
    @ResponseBody
    public List<Media> search(@RequestParam("query") String query) {

        List<Media> resultMediaList = searchService.SearchMedia(query);

        //Order alphabetically.
        Collections.sort(resultMediaList, new Comparator<Media>(){
            public int compare(Media m1, Media m2){
                return m1.getTitle().compareTo(m2.getTitle());
            }
        });

        return resultMediaList;
    }

}
