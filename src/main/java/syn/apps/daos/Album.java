package syn.apps.daos;

/**
 * Created by Patrick on 04-Jul-17.
 */
public class Album implements Media {

    private String title;
    private String artist;
    private String type = "Album";

    public Album(String title, String artist){

        this.title = title;
        this.artist = artist;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public String getAuthor() {
        return this.artist;
    }

    @Override
    public String getType() { return this.type; }
}
