package syn.apps.daos;

/**
 * Created by Patrick on 04-Jul-17.
 */
public interface Media {


    String getTitle();

    String getAuthor();

    String getType();
}
