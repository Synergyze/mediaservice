package syn.apps.daos;

/**
 * Created by Patrick on 09-Jul-17.
 */
public class HealthCheck {

    private String serviceType;
    private String healthStatus;
    private String message;
    private long responseTime;

    private HealthCheck(String healthStatus){
        this(healthStatus, "");
    }

    private HealthCheck(String healthStatus, String message){
        this.healthStatus = healthStatus;
        this.message = message;
    }

    public static HealthCheck Healthy(){
        return Healthy(0);
    }

    public static HealthCheck Healthy(long elapsedTime){

        HealthCheck health = new HealthCheck("Healthy");
        health.responseTime = elapsedTime;
        return health;
    }

    public static HealthCheck Unhealthy(){

        return Unhealthy("");
    }

    public static HealthCheck Unhealthy(String message){

        return new HealthCheck("Unhealthy", message);
    }

    public void setServiceType(String serviceType){

        this.serviceType = serviceType;
    }

    public String getServiceType(){

        return serviceType;
    }

    public String getHealthStatus(){
        return healthStatus;
    }

    public String getMessage(){
        return message;
    }

    public long getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(long responseTime) {
        this.responseTime = responseTime;
    }
}
