package syn.apps.daos;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * Created by Patrick on 04-Jul-17.
 */
//@JsonDeserialize(using = BookDeserialiser.class)
public class Book implements Media {

    private String title;
    private String author;
    private String type = "Book";

    public Book(String title, String author){

        this.title = title;
        this.author = author;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public String getAuthor() {
        return this.author;
    }

    @Override
    public String getType() {
        return this.type;
    }
}
