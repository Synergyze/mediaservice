package syn.apps.configuration;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.File;

/**
 * Created by Patrick on 06-Jul-17.
 */
@XmlRootElement(name = "Configuration")
public class Configuration {

    @XmlElement(name="BookServiceURL")
    public String bookServiceURL;

    @XmlElement(name="AlbumServiceURL")
    public String albumServiceURL;

    private static Configuration instance;
    private Configuration() {}

    public static Configuration getInstance() {
        if (instance == null) {

            // Get host specific config
            try {
                JAXBContext jc = JAXBContext.newInstance(Configuration.class);
                Unmarshaller unmarshaller = jc.createUnmarshaller();
                File configfile = new File(Configuration.class.getClassLoader().getResource("Configuration.xml").getFile());

                instance = (Configuration) unmarshaller.unmarshal(configfile);

            } catch (JAXBException exc) {
                System.out.println("Exception occurred while reading configuration.");
                System.out.println(exc);
            }
        }
        return instance;
    }
}
