package syn.apps.services.implementation;

import com.google.gson.Gson;
import syn.apps.daos.Media;
import syn.apps.servicedaos.Album;
import syn.apps.servicedaos.AlbumResultSet;
import syn.apps.daos.HealthCheck;
import syn.apps.services.MediaService;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patrick on 28-Jun-17.
 */
public class AlbumService extends MediaService {

    public AlbumService(String serviceURI) {

        this.serviceURI = serviceURI;
    }

    public List<Media> getMedia(String query) throws UnsupportedEncodingException {

        String json = get(serviceURI.replace("{0}", URLEncoder.encode(query, "UTF-8")));

        ArrayList<Media> mediaList = new ArrayList<Media>();

        Gson gson = new Gson();
        AlbumResultSet resultSet = gson.fromJson(json, AlbumResultSet.class);

        for (Album album : resultSet.results) {

            mediaList.add(new syn.apps.daos.Album(album.collectionName, album.artistName));
        }

        return mediaList;
    }

    @Override
    public HealthCheck getHealth(){

        HealthCheck health = super.getHealth();

        health.setServiceType("AlbumService");

        return health;
    }
}
