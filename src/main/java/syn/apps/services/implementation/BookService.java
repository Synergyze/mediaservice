package syn.apps.services.implementation;


import com.google.gson.Gson;
import syn.apps.daos.Media;
import syn.apps.servicedaos.Book;
import syn.apps.servicedaos.BookResultSet;
import syn.apps.daos.HealthCheck;
import syn.apps.services.MediaService;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patrick on 07-Jul-17.
 */
public class BookService extends MediaService {

    public BookService(String serviceURI) {

        this.serviceURI = serviceURI;
    }

    public List<Media> getMedia(String query) throws UnsupportedEncodingException {

        String json = get(serviceURI.replace("{0}", URLEncoder.encode(query, "UTF-8")));
        ArrayList<Media> mediaList = new ArrayList<Media>();


        Gson gson = new Gson();
        BookResultSet resultSet = gson.fromJson(json, BookResultSet.class);

        for (Book book : resultSet.items) {

            StringBuilder authorSB = new StringBuilder();

            if (book.volumeInfo != null && book.volumeInfo.authors != null && book.volumeInfo.authors.size() > 0) {

                for(String author : book.volumeInfo.authors){

                    authorSB.append(author);
                }
            }

            mediaList.add(new syn.apps.daos.Book(book.volumeInfo.title, authorSB.toString()));
        }

        return mediaList;
    }

    @Override
    public HealthCheck getHealth(){

        HealthCheck health = super.getHealth();

        health.setServiceType("BookService");

        return health;
    }
}
