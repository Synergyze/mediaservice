package syn.apps.services;

import syn.apps.daos.Media;
import syn.apps.daos.HealthCheck;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by Patrick on 28-Jun-17.
 */
public abstract class MediaService {

    protected String serviceURI;

    public abstract List<Media> getMedia(String query) throws UnsupportedEncodingException;

    public HealthCheck getHealth() {

        long startTime = System.currentTimeMillis();

        try {
            HttpURLConnection conn = openGet(serviceURI);

            if (conn.getResponseCode() != 200) {

                return HealthCheck.Unhealthy("Response code is:" + conn.getResponseCode());
            }
            conn.disconnect();
        } catch (MalformedURLException e) {

            return HealthCheck.Unhealthy("Url is malformed, " + e.getMessage());
        } catch (IOException e) {

            return HealthCheck.Unhealthy("IO Exception, " + e.getMessage());

        }

        long elapsedTime = System.currentTimeMillis() - startTime;

        return HealthCheck.Healthy(elapsedTime);
    }


    private HttpURLConnection openGet(String serviceAddress)
            throws MalformedURLException, IOException {

        URL url = new URL(serviceAddress);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

        return conn;
    }

    protected String get(String serviceAddress){

        StringBuilder resultJSONBuilder = new StringBuilder();

        try {
            HttpURLConnection conn = openGet(serviceAddress);

            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String jsonLine;
            while ((jsonLine = br.readLine()) != null) {
                resultJSONBuilder.append(jsonLine);
            }

            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();
        } catch (IOException e) {

            e.printStackTrace();
        }

        return resultJSONBuilder.toString();
    }
}
