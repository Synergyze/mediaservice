package syn.apps.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import syn.apps.daos.Media;
import syn.apps.daos.HealthCheck;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Patrick on 28-Jun-17.
 */
@Component
public class MediaSearchService {

    final private List<MediaService> services;

    @Autowired
    public MediaSearchService(List<MediaService> services) {

        this.services = services;
    }

    public List<Media> SearchMedia(String query){

        List<Media> mediaList = new ArrayList<Media>();

        for (MediaService service : services) {
            try {
                mediaList.addAll(service.getMedia(query));
            }
            catch(RuntimeException exc){
                exc.printStackTrace();
            }
            catch(UnsupportedEncodingException exc){
                exc.printStackTrace();
            }
        }

        return mediaList;
    }

    public List<HealthCheck> HealthCheck(){

        List<HealthCheck> healthChecks = new ArrayList<HealthCheck>();

        for (MediaService service : services) {
            healthChecks.add(service.getHealth());
        }

        return healthChecks;
    }
}
