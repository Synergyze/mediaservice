package syn.apps;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import syn.apps.webservices.Controller;
import syn.apps.services.MediaSearchService;

/**
 * Created by Patrick on 28-Jun-17.
 */
public class Main {

    public static void main(String[] args) {

        SpringApplication.run(Controller.class, args);
    }
}
