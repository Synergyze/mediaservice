package syn.apps.servicedaos;

import java.util.List;

/**
 * Created by Patrick on 09-Jul-17.
 */
public class BookMetadata {

    public String title;

    public List<String> authors;
}
